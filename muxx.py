import av

# Open video file for reading
container = av.open('test.mp4')

# Iterate over streams and find the video stream
video_stream = None
for stream in container.streams:
    if stream.type == 'video':
        video_stream = stream
        break

# Decode and extract frames from the video stream
for packet in container.demux(video_stream):
    for frame in packet.decode():
        # Do something with the frame
        print(f'Read frame {frame.index} from time {frame.time}')
