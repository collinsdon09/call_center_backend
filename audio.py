import serial
import pyaudio
import wave
import time

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 8000
RECORD_SECONDS = 10
WIDTH = 2

pyaudio_obj = pyaudio.PyAudio()

stream= pyaudio_obj.open(format=pyaudio_obj.get_format_from_width(WIDTH),
                channels=CHANNELS,
                rate=RATE,
                output=True,
                frames_per_buffer=CHUNK)

mic= pyaudio_obj.open(format=pyaudio.paInt16,
                channels=1,
                rate=8000,
                input=True,
                frames_per_buffer=128)

serial_obj = serial.Serial()
serial_obj.port = '/dev/ttyUSB1'
serial_obj.baudrate = 9600 # may be different
serial_obj.timeout = 0.5
serial_obj.open()
serial_obj.writeTimeout = 0

streaming = []
frames = []
while True:
 readings_from_serial= serial_obj.read(CHUNK) # reading whatever is coming from serial port
 stream.write(readings_from_serial)  #audio playback
 serial_obj.write(mic.read(612)) #sending microphone information to serial